const moment = require('moment')
const stats = require('./stats')

function create (channel) {
  return {
    channel: channel,
    lastStats: undefined,
    nextWW: undefined,
    stats: stats.read(),
    wwHumanTime () {
      if (this.nextWW && this.nextWW.time) {
        return this.nextWW.time.format('H[h]mm')
      } else {
        console.log(this)
        return '[Oops, je n\'est pas de prochaine WW…]'
      }
    },
    wwEndHumanTime () {
      if (this.nextWW && this.nextWW.time) {
        const end = this.nextWW.time.clone().add(this.nextWW.duration, 'minutes')
        return end.format('H[h]mm')
      } else {
        console.log(this)
        return '[Oops, je n\'est pas de prochaine WW…]'
      }
    },
    wwIsActive () {
      return this.nextWW && moment() > this.nextWW.time
    },
    participants (list) {
      const participants = Array.from(list)
      if (participants.length === 1) {
        return participants[0]
      } else {
        const last = participants.pop()
        return participants.join(', ') + ' et ' + last
      }
    },
    startWW () {
      console.log('Starting WW…')
      if (this.nextWW === undefined) {
        console.log('No WW to start.')
        console.log(this)
      } else if (!this.nextWW.time.isSame(moment(), 'minute')) {
        console.log('The WW is not meant to start now.')
        console.log(this)
        this.nextWW = undefined
      } else {
        this.channel.say('WW de ' + this.nextWW.duration + ' minutes dans 3…')
        setTimeout(() => {
          this.channel.say('2…')
        }, 1000)
        setTimeout(() => {
          this.channel.say('1…')
        }, 2000)
        setTimeout(() => {
          this.channel.say('Allez ' + this.participants(this.nextWW.participants) + ' ! GO ! GO ! GO ! Vous pouvez écrire « !ww rejoindre » pour participer si il y a des retardataires motivés.')
          const duration = this.nextWW.duration * 60 * 1000

          setTimeout(() => {
            this.channel.say('Fin de la WW dans 3…')
          }, duration - 3000)
          setTimeout(() => {
            this.channel.say('2…')
          }, duration - 2000)
          setTimeout(() => {
            this.channel.say('1…')
          }, duration - 1000)
          setTimeout(() => {
            this.channel.say('Fin de la WW ' + this.participants(this.nextWW.participants) + ' ! On compte ses mots !')
            this.lastStats = {
              date: this.nextWW.time,
              duration: this.nextWW.duration,
              results: emptyResults(this.nextWW.participants)
            }
            this.nextWW = undefined

            setTimeout(() => {
              this.channel.say('Vous avez écrit ' + stats.words(this.lastStats) + ' mots en ' + this.lastStats.duration + ' minutes. Félicitations à ' + this.participants(Object.keys(this.lastStats.results)) + ' !')
              stats.save(this.lastStats)
              this.stats = stats.add(this.stats, this.lastStats)
              this.lastStats = undefined
            }, 3 * 60 * 1000)
          }, duration)
        }, 3000)
      }
    }
  }
}

function emptyResults (participants) {
  var results = {}
  for (let participant of participants) {
    results[participant] = 0
  }
  return results
}

exports.create = create
