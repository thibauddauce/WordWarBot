const test = require('unit.js')
const moment = require('moment')
const bot = require('../src/bot')
const state = require('../src/state')

describe('Word War Bot', function () {
  it('parses a random message and do nothing', function () {
    let emptyState = state.create()
    let response = bot.parse(emptyState, 'UnitTest', 'Bonjour tout le monde')

    test.undefined(response.text)
      .undefined(response.action)
  })

  it('parses a start WW for longer than 30 minutes and warn the user', function () {
    let emptyState = state.create()
    let response = bot.parse(emptyState, 'UnitTest', '!ww35')

    test.string(response.text)
      .contains('plus de 30 minutes')
      .undefined(response.action)
  })

  it('parses a start WW for 0 minute and warn the user', function () {
    let emptyState = state.create()
    let response = bot.parse(emptyState, 'UnitTest', '!ww00')

    test.string(response.text)
      .contains('Un peu court 0 minute')
      .undefined(response.action)
  })

  it('parses a start WW in more than 60 minutes and warn the user', function () {
    let emptyState = state.create()
    let response = bot.parse(emptyState, 'UnitTest', '!ww15 dans 70min')

    test.string(response.text)
      .contains('je ne prévois pas les WW dans plus d\'une heure')
      .undefined(response.action)
  })

  it('parses a start WW when there is a current WW and warn the user', function () {
    let emptyState = state.create()
    emptyState.nextWW = {
      time: moment().subtract(5, 'minutes'),
      duration: 15
    }
    let response = bot.parse(emptyState, 'UnitTest', '!ww15')

    test.string(response.text)
      .contains('depuis 4 minutes')
      .undefined(response.action)
  })

  it('parses a start WW when there is a planed WW and warn the user', function () {
    let emptyState = state.create()
    emptyState.nextWW = {
      time: moment().add(5, 'minutes'),
      duration: 15
    }
    let response = bot.parse(emptyState, 'UnitTest', '!ww15')

    test.string(response.text)
      .contains('attendre un petit peu')
      .undefined(response.action)
  })

  it('parses a planned WW when there is a planed WW and warn the user', function () {
    let emptyState = state.create()
    emptyState.nextWW = {
      time: moment().add(5, 'minutes'),
      duration: 15
    }
    let response = bot.parse(emptyState, 'UnitTest', '!ww15 dans 10 min')

    test.string(response.text)
      .contains('pour me souvenir de tout')
      .undefined(response.action)
  })

  it('parses a start WW', function () {
    let wwState = state.create()
    let response = bot.parse(wwState, 'UnitTest', '!ww15')

    test.undefined(response.text)

    test.bool(wwState.nextWW.time.isSame(moment(), 'minute')).isTrue()
    test.bool(wwState.wwIsActive()).isTrue()
    test.string(wwState.wwHumanTime()).is(moment().format('H[h]mm'))
    test.array(wwState.nextWW.participants).hasValue('UnitTest')
  })

  it('parses a start WW with participants', function () {
    let wwState = state.create()
    let response = bot.parse(wwState, 'UnitTest', '!ww15 avec monAmi, fred,mamie et thomas')

    test.undefined(response.text)

    test.bool(wwState.nextWW.time.isSame(moment(), 'minute')).isTrue()
    test.bool(wwState.wwIsActive()).isTrue()
    test.string(wwState.wwHumanTime()).is(moment().format('H[h]mm'))
    test.array(wwState.nextWW.participants).hasLength(5)
  })
})
