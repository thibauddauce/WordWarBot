const fs = require('fs')

function save (lastStats) {
  fs.appendFile('stats.txt', JSON.stringify(lastStats) + '\n', function (err) {
    if (err) {
      console.log(err)
    }
  })
}

function read () {
  return fs.readFileSync('stats.txt').toString().split('\n').filter((line) => {
    return line.length > 1
  }).map((line) => {
    return JSON.parse(line)
  }).reduce(add, {
    words: 0,
    participants: {},
    duration: 0,
    participantsNumber () {
      return Object.keys(this.participants).length
    }
  })
}

function add (stats, newStats) {
  stats.words += words(newStats)

  for (let participant of Object.keys(newStats.results)) {
    stats.participants[participant] = (stats.participants[participant] || 0) + newStats.results[participant]
  }

  stats.duration += newStats.duration

  return stats
}

function words (newStats) {
  return Object.keys(newStats.results).reduce((sum, participant) => {
    return sum + newStats.results[participant]
  }, 0)
}

exports.save = save
exports.read = read
exports.add = add
exports.words = words
