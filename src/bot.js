const moment = require('moment')

function parse (state, from, text) {
  const runWWInfo = /^!ww(\d+)(( à (\d{2})h(\d{2})?)|( dans (\d{1,2})( )?(min|minutes)))?( avec (.*))?$/.exec(text)

  if (runWWInfo !== null) {
    let duration = Number.parseInt(runWWInfo[1])

    // check delay
    const hour = Number.parseInt(runWWInfo[4])
    const minutes = Number.parseInt(runWWInfo[5] || 0)
    const delay = Number.parseInt(runWWInfo[7])
    const now = moment().seconds(0)

    var start = now.clone()
    if (hour) {
      if (hour === 0 && start.hour() === 23) {
        start.add(1, 'day')
      }
      start.hour(hour).minutes(minutes)

    } else if (delay) {
      start.add(delay, 'minutes')
    }

    if (hour && hour > 23) {
      return {
        text: from + '… Je vais te faire une révélation… Il n\'y a que 24h dans une journée… Alors dépêche toi d\'avancer dans ton Nano :-)'
      }
    }

    if (minutes && minutes > 59) {
      return {
        text: from + '… Je vais te faire une révélation… Il n\'y a que 60 minutes dans une heure… Alors dépêche toi d\'avancer dans ton Nano :-)'
      }
    }

    if (start.diff(now, 'minutes') < 0) {
      return {
        text: 'Back to the future! Je ne programme pas les WW dans le passé :-)'
      }
    }

    // user error
    if (state.nextWW && state.wwIsActive()) {
      const diff = now.diff(state.nextWW.time, 'minutes')
      if (diff === 0 || diff === 1) {
        return {
          text: 'Une WW de ' + state.nextWW.duration + ' minutes vient juste de commencer ! Vas-y lance toi ! (« !ww rejoindre » si tu veux que je comptabilise tes mots à la fin) :-)'
        }
      } else {
        return {
          text: 'Il y a déjà une WW en cours depuis ' + diff + ' minutes… Tu es un peu en retard mais tu peux le faire. Je crois en toi ! (« !ww rejoindre » si tu veux que je comptabilise tes mots à la fin) :-)'
        }
      }
    }

    if (state.nextWW && (start > now)) {
      return {
        text: 'Il y a déjà une WW de prévue pour ' + state.wwHumanTime() + ', je n\'ai pas assez de mémoire pour me souvenir de tout…'
      }
    }

    if (state.nextWW) {
      return {
        text: 'Il y a déjà une WW de prévue pour ' + state.wwHumanTime() + ', est-ce que tu peux attendre un petit peu ?'
      }
    }

    if (duration === 0) {
      return {
        text: 'Un peu court 0 minute pour une WW ' + from + ', tu ne trouves pas ?'
      }
    }

    if (duration > 30) {
      return {
        text: 'Tu arrives écrire plus de 30 minutes en continu ' + from + ' ? Félicitation ! Moi pas…'
      }
    }

    if (duration !== 15 && duration !== 30) {
      return {
        text: 'Les WW c\'est 15 ou 30 minutes uniquement. L\'originalité il faut la mettre dans ton Nano pas dans les durées des WW :-)'
      }
    }

    if (start.diff(moment(), 'minutes') > 60) {
      return {
        text: 'Désolé ' + from + ' mais je ne prévois pas les WW dans plus d\'une heure. Oui… Je sais… Je suis lâche…'
      }
    }

    // save WW
    var participants = [from]
    if (runWWInfo[11]) {
      const separators = new RegExp('(, | et |,| )')
      const andWith = runWWInfo[11]
        .split(separators)
        .filter((friend) => {
          return !friend.match(separators)
        })
      participants = participants.concat(andWith)
    }

    state.nextWW = {
      from: from,
      time: start,
      duration: duration,
      participants: new Set(participants)
    }

    if (start > now) {
      return {
        text: 'La WW est prévue pour ' + state.wwHumanTime() + ' avec ' + state.participants(state.nextWW.participants) + '. Dites « !ww rejoindre » pour participer !',
        action (state) {
          setTimeout(() => {
            state.startWW()
          }, state.nextWW.time.diff(moment()))
        }
      }
    } else {
      return {
        participants: [from],
        action (state) {
          state.startWW()
        }
      }
    }
  }

  const joinWW = /^!ww (join|rejoindre)/.exec(text)
  if (joinWW) {
    if (state.nextWW) {
      if (state.nextWW.participants.has(from)) {
        return {
          text: 'Il me semble que je t\'avais déjà noté sur ma liste ' + from + '… À moins que je ne perde la tête…'
        }
      } else {
        state.nextWW.participants.add(from)
        return {
          text: 'C\'est noté ' + from + ' ! Bon courage !'
        }
      }
    } else {
      return {
        text: 'Il n\'y a pas de WW de prévue… Je veux bien en lancer une si tu veux ? Les commandes c\'est « !ww15 », « !ww30 » et tu peux ajouter « à 19h30 » si tu veux la planifier.'
      }
    }
  }

  if (state.lastStats) {
    const stats = /(\d{2,4})/.exec(text)

    if (stats) {
      if (Object.keys(state.lastStats.results).includes(from)) {
        if (state.lastStats.results[from] === 0) {
          state.lastStats.results[from] = Number.parseInt(stats[1])
        }
      } else {
        return {
          text: 'Tu participais à la WW ' + from + ' ? La prochaine fois dis le moi avec « !ww rejoindre » si tu veux que je comptabilise tes mots :-)'
        }
      }
    }
  }

  return {}
}

exports.parse = parse
