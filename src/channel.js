var irc = require('irc')

function create (name, server, channel, password) {
  const client = new irc.Client(server, name, {
    autoConnect: false
  })
  client.addListener('error', (message) => {
    console.log(message)
  })
  client.connect(() => {
    client.join(channel, () => {
      if (password) {
        client.say('Themis', 'IDENTIFY ' + password)
      }
    })
  })

  return {
    irc: client,
    say (message) {
      client.say(channel, message)
    },
    addListener (event, callback) {
      client.addListener(event, callback)
    }
  }
}

exports.create = create
