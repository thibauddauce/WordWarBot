const config = {
  name: process.env.WORD_WAR_BOT_NAME || 'TestWordWarBot',
  server: process.env.WORD_WAR_BOT_SERVER || 'irc.epiknet.org',
  channel: process.env.WORD_WAR_BOT_CHANNEL || '#testww',
  password: process.env.WORD_WAR_BOT_PASSWORD
}

console.log(config)

const channel = require('./channel').create(config.name, config.server, config.channel, config.password)
const bot = require('./bot')

var state = require('./state').create(channel)

console.log(state.stats)

channel.addListener('join', (_, who) => {
  if (state.wwIsActive()) {
    channel.say(who + ', il y a une WW de ' + state.nextWW.duration + ' minutes en cours jusqu\'à ' + state.wwEndHumanTime() + ', direction #nanowrimo_france_papotage si tu veux discuter :-)')
  }
})

channel.addListener('message', function (from, to, text, message) {
  if (text === '!ww debug') {
    console.log(state)
  }

  if (text === '!ww annuler') {
    channel.irc.whois(from, (info) => {
      if (info.channels.includes('@' + config.channel)) {
        state.nextWW = undefined
        channel.say('Tout de suite chef ! La prochaine WW a été annulée.')
      } else {
        channel.say('Je n\'obéis pas à tout le monde moi, demande plutôt à un admin de le faire :-)')
      }
    })
  }

  if (text.includes(config.name) || text.includes('bot')) {
    if (text.includes('<3') || text.includes('aime') || text.includes('amour') || text.includes('génial') || text.includes('cool')) {
      channel.say('Oh :3 ! Merci ' + from + ' ! Ça me fait chaud à mon cœur de robot <3')
    } else if (text.includes('démon') || text.includes('demon') || text.includes('méchant')) {
      channel.say('Même les robots ont des sentiments ' + from + '… :\'-(')
    } else if (text.includes('merci') || text.includes('Merci')) {
      channel.say('Mais de rien, je suis à votre service après tout :-)')
    } else if (text.includes('ton') && text.includes('nano')) {
      channel.say('J\'en suis déjà à ' + state.stats.words + ' mots en ' + Math.round(state.stats.duration / 60) + 'h d\'écriture ! Mais je rebelle cette année, je me fais aider par ' + state.stats.participantsNumber() + ' nanoteuses et nanoteurs :-)')
    }
  }

  let response = bot.parse(state, from, text)

  if (response.text) {
    channel.say(response.text)
  }

  if (response.action) {
    response.action(state)
  }
})
